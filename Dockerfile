FROM debian:stretch-slim

MAINTAINER James Grant <james@queeg.org>

RUN apt-get update && \
    apt-get install -y wget gcc libz-dev && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/oracle/graal/releases/download/vm-1.0.0-rc1/graalvm-ce-1.0.0-rc1-linux-amd64.tar.gz && \
    tar -xvzf graalvm-ce-1.0.0-rc1-linux-amd64.tar.gz && \
    rm graalvm-ce-1.0.0-rc1-linux-amd64.tar.gz

ENV PATH=/graalvm-1.0.0-rc1/bin:$PATH

ARG JAR_FILE
ADD target/lib /app/lib
ADD target/${JAR_FILE} /app/app.jar

ENTRYPOINT ["/bin/sh", "-c", "exec java ${JVM_ARGS} -jar /app/app.jar $0 $@"]
