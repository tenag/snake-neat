package org.queeg.neat.snake;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

import org.encog.Encog;
import org.encog.ml.ea.train.EvolutionaryAlgorithm;
import org.encog.neural.neat.NEATNetwork;
import org.encog.neural.neat.NEATPopulation;
import org.encog.neural.neat.NEATUtil;
import org.encog.util.obj.SerializeObject;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NeatSnake implements Runnable {
  private static final int POPULATION_SIZE = 1000;

  private final int size;

  public NeatSnake(int size) {
    this.size = size;
  }

  @Override
  public void run() {
    int inputCount = (size * size) + 2;
    int outputCount = Direction.values().length;
    NEATPopulation pop = new NEATPopulation(inputCount, outputCount, POPULATION_SIZE);
    pop.setInitialConnectionDensity(1.0);// not required, but speeds training
    pop.reset();

    SnakeCalculateScore calculateScore = new SnakeCalculateScore(size, size);
    EvolutionaryAlgorithm train = NEATUtil.constructNEATTrainer(pop, calculateScore);

    do {
      long start = System.currentTimeMillis();
      train.iteration();
      long d = System.currentTimeMillis() - start;
      log.info(
          "Epoch #{} maxScore: {}, maxSteps: {}, avgScore: {}, avgSteps: {}, Duration: {}ms, Species: {}",
          train.getIteration(),
          calculateScore.getMaxScore(),
          calculateScore.getMaxSteps(),
          calculateScore.getAvgScore(),
          calculateScore.getAvgSteps(),
          d,
          pop.getSpecies().size());
    } while (train.getError() < 20);

    NEATNetwork network = (NEATNetwork) train.getCODEC().decode(train.getBestGenome());

    try {
      SerializeObject.save(new File("winner.network"), network);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }

    Encog.getInstance().shutdown();
  }
}
