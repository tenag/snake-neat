package org.queeg.neat.snake;

public enum Direction {
  NORTH,
  EAST,
  SOUTH,
  WEST;
}