package org.queeg.neat.snake;

public enum SnakeState {
  ALIVE,
  ATE_TAIL,
  HIT_WALL,
  EXAUSTED
}