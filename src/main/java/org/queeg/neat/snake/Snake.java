package org.queeg.neat.snake;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import lombok.Getter;

public class Snake {
  private static final int    MAX_HUNGRY_MOVES = 200;
  private static final Random random           = new Random();

  private final int width;
  private final int height;

  private @Getter int        length;
  private @Getter Position   position;
  private Deque<Position>    tailParts;
  private @Getter Position   food;
  private int                movesLeft = MAX_HUNGRY_MOVES;
  private @Getter SnakeState state;

  public Snake(int width, int height) {
    checkArgument(width >= 6, "Board width must be at least 6");
    this.width = width;
    this.height = height;

    state = SnakeState.ALIVE;
    length = 4;
    int midX = width / 2;
    int midY = height / 2;
    position = new Position(midX, midY);
    tailParts = new LinkedList<>();
    tailParts.add(new Position(midX - 3, midY));
    tailParts.add(new Position(midX - 2, midY));
    tailParts.add(new Position(midX - 1, midY));
    food = positionFood();
  }

  public boolean move(Direction direction) {
    if (state != SnakeState.ALIVE) return false;

    Position newPosition = position.move(direction);

    if (outsideArea(newPosition)) {
      state = SnakeState.HIT_WALL;
      return false;
    }

    if (tailParts.contains(newPosition)) {
      state = SnakeState.ATE_TAIL;
      return false;
    }

    performMove(newPosition);

    if (movesLeft <= 0) {
      state = SnakeState.EXAUSTED;
      return false;
    }

    return state == SnakeState.ALIVE;
  }

  public List<Position> getTail() {
    return new ArrayList<>(tailParts);
  }

  private void performMove(Position newPosition) {
    tailParts.add(position);
    position = newPosition;
    if (newPosition.equals(food)) {
      length++;
      food = positionFood();
      movesLeft = MAX_HUNGRY_MOVES;
    } else {
      tailParts.remove();
      movesLeft--;
    }
  }

  private boolean outsideArea(Position position) {
    return position.getX() < 0 || position.getY() < 0 || position.getX() >= width || position.getY() >= height;
  }

  private Position positionFood() {
    Position foodPosition;
    do {
      int x, y;
      x = random.nextInt(width);
      y = random.nextInt(height);
      foodPosition = new Position(x, y);
    } while (tailParts.contains(foodPosition) || position.equals(foodPosition));
    return foodPosition;
  }
}
