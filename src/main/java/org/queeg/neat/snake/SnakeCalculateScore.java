package org.queeg.neat.snake;

import java.util.concurrent.atomic.AtomicInteger;

import org.encog.ml.CalculateScore;
import org.encog.ml.MLMethod;
import org.encog.ml.MLRegression;
import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SnakeCalculateScore implements CalculateScore {
  private final int width;
  private final int height;

  private final AtomicInteger    maxSteps = new AtomicInteger();
  private final AtomicInteger    maxScore = new AtomicInteger();
  private final DoubleRingBuffer avgSteps = new DoubleRingBuffer(1000);
  private final DoubleRingBuffer avgScore = new DoubleRingBuffer(1000);

  @Override
  public double calculateScore(MLMethod method) {
    MLRegression net = (MLRegression) method;
    Snake snake = new Snake(width, height);
    SnakeMlUtils mlUtils = new SnakeMlUtils(width, height);

    int steps = 0;
    while (snake.getState() == SnakeState.ALIVE) {
      steps++;
      BasicMLData input = mlUtils.renderInput(snake);
      MLData output = net.compute(input);
      snake.move(directionFromOutput(output));
    }

    maxSteps.getAndAccumulate(steps, Math::max);
    maxScore.getAndAccumulate(snake.getLength(), Math::max);
    avgSteps.put(steps);
    avgScore.put(snake.getLength());

    switch (snake.getState()) {
    case EXAUSTED:
      return 0.0;
    default:
      return snake.getLength() - 4 /*+ (steps / 1000.0)*/;
    }
  }

  public int getMaxSteps() {
    return maxSteps.get();
  }

  public int getMaxScore() {
    return maxScore.get();
  }

  public double getAvgSteps() {
    return avgSteps.average();
  }

  public double getAvgScore() {
    return avgScore.average();
  }

  private Direction directionFromOutput(MLData o) {
    int maxIndex = maxIndex(o.getData());
    return Direction.values()[maxIndex];
  }

  private int maxIndex(double[] array) {
    int maxIndex = 0;
    double maxFound = array[0];
    for (int i = 1; i < array.length; i++) {
      if (array[i] > maxFound) {
        maxFound = array[i];
        maxIndex = i;
      }
    }
    return maxIndex;
  }

  @Override
  public boolean shouldMinimize() {
    return false;
  }

  @Override
  public boolean requireSingleThreaded() {
    return false;
  }
}
