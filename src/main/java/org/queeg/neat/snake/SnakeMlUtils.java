package org.queeg.neat.snake;

import org.encog.ml.data.basic.BasicMLData;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SnakeMlUtils {
  private final int width;
  private final int height;

  public BasicMLData renderInput(Snake snake) {
    Position position = snake.getPosition();
    Position food = snake.getFood();

    double[] input = new double[(width * 2 - 1) * (height * 2 - 1) + 2];
    for (int y = 0; y < height * 2 - 1; y++) {
      for (int x = 0; x < width * 2 - 1; x++) {
        if ((x < width - position.getX() - 1) || (height - position.getY() - 1 > y)
            || (x >= 2 * width - position.getX() - 1) || (y >= 2 * height - position.getY() - 1)) {
          input[y * (width * 2 - 1) + x] = 1.0;
        }
      }
    }

    input[positionOffset(position, position)] = 1.0;
    snake.getTail().forEach(tailPart -> input[positionOffset(position, tailPart)] = 1.0);

    input[(width * 2 - 1) * (height * 2 - 1)] = (food.getX() - position.getX()) / width;
    input[(width * 2 - 1) * (height * 2 - 1) + 1] = (food.getY() - position.getY()) / height;

    return new BasicMLData(input);
  }

  public int positionOffset(Position origin, Position pos) {
    int xDiff = pos.getX() - origin.getX();
    int yDiff = pos.getY() - origin.getY();

    int x = width + xDiff - 1;
    int y = height + yDiff - 1;

    return (width * 2 - 1) * y + x;
  }
}
