package org.queeg.neat.snake;

public class DoubleRingBuffer {
  private final double[] elements;

  private int writePos;

  public DoubleRingBuffer(int capacity) {
    elements = new double[capacity];

    writePos = 0;
  }

  public void put(double value) {
    elements[writePos] = value;
    writePos = (writePos + 1) % elements.length;
  }

  public double average() {
    double total = 0.0;
    for (double v : elements) {
      total += v;
    }
    return total / elements.length;
  }
}
