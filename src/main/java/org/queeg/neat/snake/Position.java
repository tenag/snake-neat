package org.queeg.neat.snake;

import lombok.NonNull;
import lombok.Value;

@Value
public class Position {
  int x;
  int y;

  public Position move(@NonNull Direction direction) {
    switch (direction) {
    case EAST:
      return new Position(x + 1, y);
    case NORTH:
      return new Position(x, y + 1);
    case SOUTH:
      return new Position(x, y - 1);
    case WEST:
      return new Position(x - 1, y);
    default:
      throw new IllegalArgumentException("Unknown Direction " + direction);
    }
  }
}