package org.queeg.neat.snake;

public class NeatSnakeApp {
  public static void main(String[] args) throws Exception {
    NeatSnake neatSnake = new NeatSnake(20);
    neatSnake.run();
  }
}
