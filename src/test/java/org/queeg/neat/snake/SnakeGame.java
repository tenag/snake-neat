package org.queeg.neat.snake;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;

public class SnakeGame implements Runnable {
  private final int width;
  private final int height;

  public SnakeGame(int width, int height) {
    this.width = width;
    this.height = height;
  }

  @Override
  public void run() {
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

      Snake snake = new Snake(width, height);
      int steps = 0;
      while (snake.getState() == SnakeState.ALIVE) {
        steps++;
        System.out.println("Step: " + steps + ", Score: " + snake.getLength());
        printSnake(snake);
        System.out.print(" > ");
        String cmd = reader.readLine();
        Direction direction = Direction.valueOf(cmd);
        boolean alive = snake.move(direction);
        if (!alive) {
          System.out.println("You Died!");
          return;
        }
      }
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private void printSnake(Snake snake) {
    for (int y = height - 1; y >= 0; y--) {
      for (int x = 0; x < width; x++) {
        Position p = new Position(x, y);
        if (p.equals(snake.getPosition())) {
          System.out.print('H');
        } else if (snake.getTail().contains(p)) {
          System.out.print('T');
        } else if (p.equals(snake.getFood())) {
          System.out.print('*');
        } else {
          System.out.print('.');
        }
      }
      System.out.println();
    }
    // double[] data = mlUtils.renderInput(snake).getData();
    //
    // for (int i = 0; i < data.length; i++) {
    // if (i % (width * 2 - 1) == 0 && i != 0) {
    // System.out.println();
    // } else if (i != 0) {
    // System.out.print(", ");
    // }
    // System.out.print(String.format("%.0f", data[i]));
    // }
    // System.out.println();
    //
  }

  public static void main(String args[]) throws Exception {
    new SnakeGame(20, 20).run();
  }
}
