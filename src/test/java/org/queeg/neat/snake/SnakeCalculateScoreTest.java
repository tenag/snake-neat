package org.queeg.neat.snake;

import org.junit.Test;

public class SnakeCalculateScoreTest {
  @Test
  public void testName() throws Exception {
    int size = 11;

    Snake snake = new Snake(size, size);
    // snake.move(Direction.NORTH);
    // snake.move(Direction.WEST);
    // snake.move(Direction.NORTH);
    SnakeMlUtils mlUtils = new SnakeMlUtils(size, size);
    double[] data = mlUtils.renderInput(snake).getData();

    for (int i = 0; i < data.length; i++) {
      if (i % (size * 2 - 1) == 0 && i != 0) {
        System.out.println();
      } else if (i != 0) {
        System.out.print(", ");
      }
      System.out.print(String.format("%.0f", data[i]));
    }
    System.out.println();
  }
}
